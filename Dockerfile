FROM golang:1.22.2-alpine3.19 as builder
LABEL authors="ff755"
ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn
WORKDIR /app
COPY . /app
RUN GOOS=linux GOARCH=amd64 go build -o app

FROM alpine:3.19.1
WORKDIR /app
COPY --from=builder /app .

EXPOSE 8080

#CMD sh -c "sleep 5s && ./app"
CMD ./app